project  : argentumAg
Run as SprigBoot app.

Create order : 
	Url : http://localhost:8080/argentum/
	Method : Post
	input Body : JSON
	input example : {"userId":"JP1","product":"3.5KG","quantity":1.0,"pricePerUnit":20.95,"type":"SELL","status":"init"}

Get dashboard : 
	Url : http://localhost:8080/argentum/dashboard
	Method : Get

//input
[
	{"userId":"JP1","product":"3.5KG","quantity":1.0,"pricePerUnit":20.95,"type":"SELL","status":"init"},
	{"userId":"JP1","product":"2.5KG","quantity":2.0,"pricePerUnit":16.95,"type":"SELL","status":"init"},
	{"userId":"JP1","product":"1KG","quantity":1.0,"pricePerUnit":10.95,"type":"SELL","status":"init"},
	{"userId":"JP2","product":"3.5KG","quantity":1.0,"pricePerUnit":20.95,"type":"SELL","status":"init"},
	{"userId":"JP2","product":"2.5KG","quantity":3.0,"pricePerUnit":16.95,"type":"SELL","status":"init"},
	{"userId":"JP3","product":"3.5KG","quantity":1.0,"pricePerUnit":20.95,"type":"SELL","status":"init"},
	{"userId":"JP3","product":"2.5KG","quantity":4.0,"pricePerUnit":16.95,"type":"SELL","status":"init"},
	{"userId":"JP3","product":"3.5KG","quantity":1.0,"pricePerUnit":19.95,"type":"BUY","status":"init"},
	{"userId":"JP3","product":"2.5KG","quantity":3.0,"pricePerUnit":15.95,"type":"BUY","status":"init"}]
// output : merged quantity sell list on price tag ascending and buy list in price tag descending
{
	"sellList":[
		{"quantity":1.0,"priceTag":10.95,"type":"SELL"},
		{"quantity":9.0,"priceTag":16.95,"type":"SELL"},
		{"quantity":3.0,"priceTag":20.95,"type":"SELL"}],
	"buyList":[
		{"quantity":1.0,"priceTag":19.95,"type":"BUY"},
		{"quantity":3.0,"priceTag":15.95,"type":"BUY"}]
}