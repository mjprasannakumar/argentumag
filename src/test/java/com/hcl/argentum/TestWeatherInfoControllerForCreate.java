package com.hcl.argentum;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.hcl.argentum.controller.WeatherAnalyzerController;
import com.hcl.argentum.domain.WeatherInfo;
import com.hcl.argentum.service.WeatherInfoService;

@RunWith(MockitoJUnitRunner.class)
public class TestWeatherInfoControllerForCreate {

	WeatherInfo wheatherInfo;
	
	@InjectMocks
	WeatherAnalyzerController wheatherAnalyzerController;
	
	@Mock
	WeatherInfoService weatherInfoService;
	
	@Before
	public void prepareWheatherInfo(){
		wheatherInfo = new WeatherInfo();
		wheatherInfo.setHumidity(99);
		wheatherInfo.setPlace("Delhi");
		wheatherInfo.setTemperature(11);
		wheatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));;
	}
	
	@Test
	public void test() {
		
		wheatherAnalyzerController.createWheatherInfo(wheatherInfo);
	}

}
