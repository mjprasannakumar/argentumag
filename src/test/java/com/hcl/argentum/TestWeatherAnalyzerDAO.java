package com.hcl.argentum;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hcl.argentum.controller.WeatherAnalyzerController;
import com.hcl.argentum.dao.WeatherInfoDAOImpl;
import com.hcl.argentum.domain.WeatherInfo;


@RunWith(MockitoJUnitRunner.class)
public class TestWeatherAnalyzerDAO {

	WeatherInfo weatherInfo;
	
	@InjectMocks
	WeatherInfoDAOImpl wheatherInfoDAO ;
	
	@Mock
	JdbcTemplate jdbcTemplate;
	
	@InjectMocks
	WeatherAnalyzerController wheatherAnalyzerController;
	
	@Before
	public void getMockWeatherInfo(){
		weatherInfo = new WeatherInfo();
		weatherInfo.setId(999);
		weatherInfo.setHumidity(87);
		weatherInfo.setTemperature(34.2);
		weatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		weatherInfo.setPlace("brl");
	}
	
	@Test
	public void testWheatherInfoDao() {
		String insertQuery = "INSERT INTO WEATHERINFO VALUES (?,?,?,?,?)";
		String maxIdQuery = "SELECT MAX(ID) FROM WEATHERINFO";
		Mockito.when(jdbcTemplate.update(insertQuery , new Object[] { 999 , weatherInfo.getPlace() , weatherInfo.getTemperature() ,
				weatherInfo.getHumidity() , weatherInfo.getTimeStamp() })).thenReturn(1);
		Mockito.when(jdbcTemplate.queryForObject(maxIdQuery, Integer.class)).thenReturn(999);
		wheatherInfoDAO.createWeatherInfo(weatherInfo);
		
	}
	
	
	/*@Test(expected = InvalidWeatherInfoException.class)
	public void testWheatherInfoDaoInValidWeatherInfoData(){
		weatherInfo.setHumidity(890);
		weatherInfo.setTemperature(89);
		weatherInfo.setPlace("");
		weatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		
		wheatherInfoDAO.createWeatherInfo(weatherInfo);
		
	}*/
}
