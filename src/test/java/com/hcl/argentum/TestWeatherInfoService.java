package com.hcl.argentum;

import static org.junit.Assert.*;
import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.hcl.argentum.dao.WeatherInfoDAO;
import com.hcl.argentum.domain.Cloud;
import com.hcl.argentum.domain.Coordination;
import com.hcl.argentum.domain.MainInfo;
import com.hcl.argentum.domain.SystemInfo;
import com.hcl.argentum.domain.Weather;
import com.hcl.argentum.domain.WeatherInfo;
import com.hcl.argentum.domain.WeatherInfoComponent;
import com.hcl.argentum.domain.WindComponent;
import com.hcl.argentum.service.WeatherInfoServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TestWeatherInfoService {
//Testing GIT 
	WeatherInfoComponent weatherInfoComponent = null;

	@Mock
	WeatherInfoDAO weatherInfoDAO;

	@InjectMocks
	WeatherInfoServiceImpl weatherInfoServiceImpl;

	@Test
	public void testCreateWheatherInfo() {

		Mockito.when(weatherInfoDAO.createWeatherInfo(Mockito.any(WeatherInfo.class))).thenReturn(new WeatherInfo());

		WeatherInfoComponent weatherInfoComponent = weatherInfoServiceImpl.getWeatherInfoComponent();
		
		assertEquals(weatherInfoComponent.getBase(), weatherInfoComponent.getBase());
		assertEquals(weatherInfoComponent.getCod(), weatherInfoComponent.getCod());
		assertEquals(weatherInfoComponent.getId(), weatherInfoComponent.getId());
		assertEquals(weatherInfoComponent.getName(), weatherInfoComponent.getName());
		assertEquals(weatherInfoComponent.getVisibility(), weatherInfoComponent.getVisibility());
		assertTrue(weatherInfoComponent.getCoord().getLat() == weatherInfoComponent.getCoord().getLat());
		assertEquals(weatherInfoComponent.getClouds().getAll(), weatherInfoComponent.getClouds().getAll());
		assertEquals(weatherInfoComponent.getSys().getCountry(), weatherInfoComponent.getSys().getCountry());
		assertEquals(weatherInfoComponent.getWeather()[0].getDescription(),
				weatherInfoComponent.getWeather()[0].getDescription());
	}

	@Before
	public void populateWeatherComponent() {
		weatherInfoComponent = new WeatherInfoComponent();
		Cloud cloud = new Cloud();
		cloud.setAll(90);
		weatherInfoComponent.setClouds(cloud);

		Coordination coord = new Coordination();
		coord.setLon(new Double(-0.13));
		coord.setLat(51.51);
		weatherInfoComponent.setCoord(coord);

		MainInfo main = new MainInfo();
		main.setHumidity(81);
		main.setPressure(1012);
		main.setTemp(289.11);
		main.setTemp_max(289.2);
		main.setTemp_min(270.99);
		weatherInfoComponent.setMain(main);

		Weather weather = new Weather();
		weather.setId(1212);
		weather.setDescription("light intensity drizzle");
		weather.setIcon("090D");
		weather.setMain("Drizzle");
		Weather[] weathers = new Weather[1];
		weathers[0] = weather;
		weatherInfoComponent.setWeather(weathers);

		SystemInfo sys = new SystemInfo();
		sys.setCountry("GB");
		sys.setId(12111);
		sys.setMessage("0.01003");
		sys.setType(1);
		sys.setSunrise(1485762037);
		sys.setSunset(1485794875);
		weatherInfoComponent.setSys(sys);

		WindComponent wind = new WindComponent();
		wind.setDeg(99.00);
		wind.setSpeed(78.9);
		weatherInfoComponent.setWind(wind);

		weatherInfoComponent.setBase("stations");
		weatherInfoComponent.setDt(new Timestamp(System.currentTimeMillis()));
		weatherInfoComponent.setId(2643743);
		weatherInfoComponent.setName("London");
		weatherInfoComponent.setCod(200);
		weatherInfoComponent.setVisibility(10000);
	}

}
