package com.hcl.argentum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hcl.argentum.domain.WeatherInfo;
import com.hcl.argentum.service.WeatherInfoService;

/**

* Controller for Weather Analyzer >>>>>>>>>>>>>>>>>>
*

**/

@Controller
@RequestMapping(value = "/wheatherinfo")
public class WeatherAnalyzerController {
	
	@Autowired
	WeatherInfoService weatherInfoService;
	
	@RequestMapping(value = "/create",method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE )
	public ResponseEntity<WeatherInfo> createWheatherInfo(@RequestBody WeatherInfo wheatherInfo){
		weatherInfoService.createWheatherInfo(wheatherInfo);
		return new ResponseEntity<>(wheatherInfo, HttpStatus.CREATED);
	}
}
