package com.hcl.argentum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hcl.argentum.domain.Order;
import com.hcl.argentum.domain.WeatherInfo;
import com.hcl.argentum.service.OrderService;
import com.hcl.argentum.service.WeatherInfoService;

/**

* Controller for Order >>>>>>>>>>>>>>>>>>
*

**/

@Controller
@RequestMapping(value = "/order")
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	@RequestMapping(value = "/",method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE )
	public ResponseEntity<Order> createOrder(@RequestBody Order order){
		orderService.createOrder(order);
		return new ResponseEntity<>(order, HttpStatus.CREATED);
	}
}
