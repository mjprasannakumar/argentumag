package com.hcl.argentum.domain;

import java.sql.Timestamp;

public class WeatherInfoComponent {
	
	Coordination coord = null;
	Weather weather[] = null;
	String base = null;
	MainInfo main = null;
	int visibility;
	WindComponent wind = null;
	Cloud clouds = null;
	Timestamp dt;
	SystemInfo sys = null;
	int id;
	String name;
	int cod;
	public Coordination getCoord() {
		return coord;
	}
	public void setCoord(Coordination coord) {
		this.coord = coord;
	}
	public Weather[] getWeather() {
		return weather;
	}
	public void setWeather(Weather[] weather) {
		this.weather = weather;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public MainInfo getMain() {
		return main;
	}
	public void setMain(MainInfo main) {
		this.main = main;
	}
	public int getVisibility() {
		return visibility;
	}
	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}
	public WindComponent getWind() {
		return wind;
	}
	public void setWind(WindComponent wind) {
		this.wind = wind;
	}
	public Cloud getClouds() {
		return clouds;
	}
	public void setClouds(Cloud clouds) {
		this.clouds = clouds;
	}
	public Timestamp getDt() {
		return dt;
	}
	public void setDt(Timestamp dt) {
		this.dt = dt;
	}
	public SystemInfo getSys() {
		return sys;
	}
	public void setSys(SystemInfo sys) {
		this.sys = sys;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	
	

}
