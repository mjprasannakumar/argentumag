package com.hcl.argentum.exception;

public class NoUserFound extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoUserFound(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
