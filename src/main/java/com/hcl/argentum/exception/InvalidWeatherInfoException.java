package com.hcl.argentum.exception;

public class InvalidWeatherInfoException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	public InvalidWeatherInfoException(String message){
		this.message = message;
	}
}
