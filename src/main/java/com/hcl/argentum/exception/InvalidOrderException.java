package com.hcl.argentum.exception;

public class InvalidOrderException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	public InvalidOrderException(String message){
		this.message = message;
	}
}
