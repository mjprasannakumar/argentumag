package com.hcl.argentum.dao;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hcl.argentum.domain.WeatherInfo;
import com.hcl.argentum.exception.InvalidWeatherInfoException;

@Repository("wheatherInfoDao")
public class WeatherInfoDAOImpl implements WeatherInfoDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public WeatherInfo createWeatherInfo(WeatherInfo wheatherInfo) {

		int maxID;
		try {
			String insertQuery = "INSERT INTO WEATHERINFO VALUES (?,?,?,?,?)";
			maxID = 100;
			jdbcTemplate.update(insertQuery, wheatherInfo);
		} catch (DataAccessException e) {
			throw new InvalidWeatherInfoException(null); 
		}

		return populateWheatherInfo(wheatherInfo, maxID);
	}

	private WeatherInfo populateWheatherInfo(WeatherInfo wheatherInfo, int wheatherInfoId) {
		WeatherInfo newWheatherInfo = new WeatherInfo();
		BeanUtils.copyProperties(wheatherInfo, newWheatherInfo);
		newWheatherInfo.setId(wheatherInfoId);
		return newWheatherInfo;
	}
}
