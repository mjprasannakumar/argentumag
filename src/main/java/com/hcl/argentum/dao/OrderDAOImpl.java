package com.hcl.argentum.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.hcl.argentum.domain.Order;
import com.hcl.argentum.exception.InvalidOrderException;

public class OrderDAOImpl implements OrderDAO {

	// user specific order in a cache
	protected static Map<String, Order> orderMap = new HashMap<String, Order>();
	
	public Order insertOrder(Order order){
		Order orderNew = new Order();
		try {
			// Here goes the order id generation logic, at this moment using random UUID is given to order Id
			String uniqId = UUID.randomUUID().toString();
			order.setOrderId(uniqId);
			orderNew = orderMap.get(order.getUserId());
			if (orderNew != null) {
				throw new InvalidOrderException("Order already exists!!!");
			} else {
				orderMap.put(order.getUserId(), order);
			}
		} catch (Exception ex) {
			throw new InvalidOrderException(ex.getMessage());
		}
		return orderNew;
	}
}
