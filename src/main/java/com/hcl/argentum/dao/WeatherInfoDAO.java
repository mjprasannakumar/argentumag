package com.hcl.argentum.dao;

import com.hcl.argentum.domain.WeatherInfo;


public interface WeatherInfoDAO {

	public WeatherInfo createWeatherInfo(WeatherInfo wheatherInfo);
	
}
