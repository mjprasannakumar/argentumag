package com.hcl.argentum.dao;

import com.hcl.argentum.domain.Order;

public interface OrderDAO {
	public Order insertOrder(Order order);
}
