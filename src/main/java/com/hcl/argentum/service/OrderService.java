package com.hcl.argentum.service;

import com.hcl.argentum.domain.Order;

public interface OrderService {

	public Order createOrder(Order order);
}
