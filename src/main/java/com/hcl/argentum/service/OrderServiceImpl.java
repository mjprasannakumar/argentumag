package com.hcl.argentum.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hcl.argentum.dao.OrderDAO;
import com.hcl.argentum.dao.WeatherInfoDAO;
import com.hcl.argentum.domain.Order;
import com.hcl.argentum.domain.WeatherInfo;
import com.hcl.argentum.domain.WeatherInfoComponent;
import com.hcl.argentum.exception.InvalidOrderException;
import com.hcl.argentum.exception.InvalidWeatherInfoException;

@Service
public class OrderServiceImpl implements OrderService{
	
	@Autowired 
	OrderDAO orderDAO;

	public Order createOrder(Order order){
		Order orderLocal = null;
		
		Order orderReturned = null;
		try {
			
			orderReturned = orderDAO.createOrder(order);
		} catch (InvalidOrderException e) {
			throw new InvalidOrderException("Invalid Order info");
		}
		return orderReturned;
	}
	
	public WeatherInfoComponent getWeatherInfoComponent() {
		
		WeatherInfoComponent weatherInfoComponent = null;
		RestTemplate restTemplate = new RestTemplate();
		String url = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	     
	    ResponseEntity<WeatherInfoComponent> result = restTemplate.exchange(url, HttpMethod.GET, entity, WeatherInfoComponent.class);
	    weatherInfoComponent = result.getBody();
		return weatherInfoComponent;
	}
}
