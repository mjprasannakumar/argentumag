package com.hcl.argentum.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hcl.argentum.dao.WeatherInfoDAO;
import com.hcl.argentum.domain.WeatherInfo;
import com.hcl.argentum.domain.WeatherInfoComponent;
import com.hcl.argentum.exception.InvalidWeatherInfoException;

@Service
public class WeatherInfoServiceImpl implements WeatherInfoService{
	
	@Autowired 
	WeatherInfoDAO wheatherInfoDAO;

	public WeatherInfo createWheatherInfo(WeatherInfo weatherInfo){
		WeatherInfoComponent weatherInfoComponent = getWeatherInfoComponent();
		
		WeatherInfo weatherInfoReturned = null;;
		try {
			WeatherInfo weatherInfoNew = new WeatherInfo();
			weatherInfoNew.setId(weatherInfoComponent.getId());
			weatherInfoNew.setHumidity(weatherInfoComponent.getMain().getHumidity());
			weatherInfoNew.setPlace(weatherInfoComponent.getName());
			weatherInfoNew.setTemperature(weatherInfoComponent.getMain().getTemp());
			weatherInfoReturned = wheatherInfoDAO.createWeatherInfo(weatherInfoNew);
		} catch (InvalidWeatherInfoException e) {
			throw new InvalidWeatherInfoException("Invalid Weather Info");
		}
		return weatherInfoReturned;
	}
	
	public WeatherInfoComponent getWeatherInfoComponent() {
		
		WeatherInfoComponent weatherInfoComponent = null;
		RestTemplate restTemplate = new RestTemplate();
		String url = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	     
	    ResponseEntity<WeatherInfoComponent> result = restTemplate.exchange(url, HttpMethod.GET, entity, WeatherInfoComponent.class);
	    weatherInfoComponent = result.getBody();
		return weatherInfoComponent;
	}
}
